from flask import Flask
from .models import db
from .models import mg
from environs import Env

from .views import configure as views_configure


def create_app(mode='development'):
    env = Env()
    env.read_env()

    app = Flask(__name__)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = env.bool('SQLALCHEMY_TRACK_MODIFICATIONS')
    app.config['SQLALCHEMY_DATABASE_URI'] = env.str('SQLALCHEMY_DATABASE_URI')
    
    db.init_app(app)
    mg.init_app(app, db)


    views_configure(app)

    return app
